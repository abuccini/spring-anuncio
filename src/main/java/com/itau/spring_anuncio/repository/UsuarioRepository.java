package com.itau.spring_anuncio.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import com.itau.spring_anuncio.model.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer>{
	public Optional<Usuario> findByEmail(String email);

}
