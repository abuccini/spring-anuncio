package com.itau.spring_anuncio.repository;

import org.springframework.data.repository.CrudRepository;
import com.itau.spring_anuncio.model.Oferta;

public interface OfertaRepository extends CrudRepository<Oferta, Integer>{

}
