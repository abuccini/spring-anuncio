package com.itau.spring_anuncio.repository;

import org.springframework.data.repository.CrudRepository;
import com.itau.spring_anuncio.model.Anuncio;

public interface AnuncioRepository extends CrudRepository<Anuncio, Integer> {

}
