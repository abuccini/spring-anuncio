package com.itau.spring_anuncio.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.itau.spring_anuncio.model.Oferta;
import com.itau.spring_anuncio.repository.OfertaRepository;

public class OfertaController {
	@Autowired
	OfertaRepository ofertaRepository;
	@RequestMapping(path="/oferta", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Oferta> getOferta() {
		return ofertaRepository.findAll();
	}

	@RequestMapping(path="/oferta/{id}", method=RequestMethod.GET)
	@ResponseBody
	public Optional<Oferta> getOfertaById(@PathVariable(value="id")  int id) {
		return ofertaRepository.findById(id);
	}
	
	@RequestMapping(path="/oferta", method=RequestMethod.POST)
	@ResponseBody
	public Oferta insertUsuario(@RequestBody Oferta oferta) {
		return ofertaRepository.save(oferta);
		}
}

