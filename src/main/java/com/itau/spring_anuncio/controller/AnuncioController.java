package com.itau.spring_anuncio.controller;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.itau.spring_anuncio.model.Anuncio;
import com.itau.spring_anuncio.model.Usuario;
import com.itau.spring_anuncio.repository.AnuncioRepository;
import com.itau.spring_anuncio.repository.UsuarioRepository;

@Controller
public class AnuncioController {
	@Autowired
	AnuncioRepository anuncioRepository;
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@RequestMapping(path="/anuncio", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Anuncio> getAnuncio(){
		return anuncioRepository.findAll();
	}

	@RequestMapping(path="/anuncio/{id}", method=RequestMethod.GET)
	@ResponseBody
	public Optional<Anuncio> getAnuncioById(@PathVariable(value="id")  int id) {
		return anuncioRepository.findById(id);
	}
	
	@RequestMapping(path="/anuncio/", method=RequestMethod.POST)
	@ResponseBody
	public Anuncio insertAnuncio(@RequestBody Anuncio anuncio) {
		return anuncioRepository.save(anuncio);
		
	}

}