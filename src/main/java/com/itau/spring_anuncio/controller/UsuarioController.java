package com.itau.spring_anuncio.controller;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.itau.spring_anuncio.model.Usuario;
import com.itau.spring_anuncio.repository.UsuarioRepository;

@Controller
public class UsuarioController {
	@Autowired
	UsuarioRepository usuarioRepository;
	BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

	@RequestMapping(path="/usuario", method=RequestMethod.GET)
	@ResponseBody
	public Iterable<Usuario> getUsuario() {
		return usuarioRepository.findAll();
	}

	@RequestMapping(path="/usuario/{id}", method=RequestMethod.GET)
	@ResponseBody
	public Optional<Usuario> getUsuarioById(@PathVariable(value="id")  int id) {
		return usuarioRepository.findById(id);
	}

	@RequestMapping(path="/usuario", method=RequestMethod.POST)
	@ResponseBody
	public Usuario insertUsuario(@RequestBody Usuario usuario) {
		return usuarioRepository.save(usuario);
	}

	@RequestMapping(path="/usuario/cadastrar", method=RequestMethod.POST)
	public @ResponseBody Usuario salvar(@RequestBody Usuario usuario) {
		String hash = encoder.encode(usuario.getSenha());
		usuario.setSenha(hash);
		return usuarioRepository.save(usuario);
	}

	@RequestMapping(path="/usuario/login", method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<?>  logar(@RequestBody Usuario usuario) {
		Optional<Usuario> usuarioBanco = usuarioRepository.findByEmail(usuario.getEmail());

		if(! usuarioBanco.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		boolean deuCerto = encoder.matches(usuario.getSenha(), usuarioBanco.get().getSenha());
		if(deuCerto) {
			HttpHeaders headers = new HttpHeaders();
			return new ResponseEntity<Usuario>(usuarioBanco.get(), headers, HttpStatus.OK);
		}	
		return ResponseEntity.badRequest().build();

	}
}