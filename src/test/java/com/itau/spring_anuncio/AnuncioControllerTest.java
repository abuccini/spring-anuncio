package com.itau.spring_anuncio;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.itau.spring_anuncio.controller.AnuncioController;
import com.itau.spring_anuncio.model.Anuncio;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class AnuncioControllerTest {
	@LocalServerPort
	int port;
	
	@Autowired
	TestRestTemplate restTemplate;
	
	@Autowired
	AnuncioController anuncioController;
	
	@Test
	public void retornarAnuncio() {
		String url = String.format("http:\\localhost:%s", port);
		String anuncioTest = restTemplate.getForObject(url,String.class);	
		Anuncio anuncio = new Anuncio();
		anuncio.setTitulo("Anuncio1");
	}

}
